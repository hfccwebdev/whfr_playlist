<?php

namespace Drupal\whfr_playlist\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for WHFR Playlist entity forms.
 */
class WhfrPlaylistForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $message_arguments = ['%label' => $this->entity->label()];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New playlist entry %label has been created.', $message_arguments));
      $this->logger('whfr_playlist')->notice('Created playlist entry %label', $message_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('Playlist entry %label has been updated.', $message_arguments));
      $this->logger('whfr_playlist')->notice('Updated playlist entry %label.', $message_arguments);
    }

    $form_state->setRedirect('entity.whfr_playlist.canonical', ['whfr_playlist' => $entity->id()]);
  }

}
