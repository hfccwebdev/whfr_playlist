<?php

namespace Drupal\whfr_playlist\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class WhfrPlaylistForm.
 *
 * @ingroup whfr_playlist
 */
class WhfrPlaylistSettingsForm extends ConfigFormBase {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'whfr_playlist.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'whfr_playlist_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::SETTINGS);

    $form['message']['#markup'] = 'Administration page for WHFR Playlist entities.';

    $form['whfr_playlist_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Playlist Settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $genres = '';
    foreach ($config->get('genre_list') as $key => $value) {
      $genres .= "$key|$value" . PHP_EOL;
    }

    $form['whfr_playlist_settings']['genre_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Genre List'),
      '#rows' => 12,
      '#default_value' => $genres,
      '#description' => $this->t('
        The possible values this field can contain. Enter one value per line, in the format key|label.
        The key is the value that will be stored in the database, and it must match the field storage type (text).
        The label is optional, and the key will be used as the label if no label is specified.
      '),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $genres = $form_state->getValue('genre_list');
    $genre_list = [];
    foreach (explode(PHP_EOL, $genres) as $row) {
      if (!empty(trim($row))) {
        $row = trim($row);
        if (strpos($row, '|')) {
          [$key, $value] = explode('|', $row);
          $genre_list[trim($key)] = trim($value);
        }
        else {
          $genre_list[trim($row)] = trim($row);
        }
      }
    }

    ksort($genre_list);

    // @todo check existing data and refuse to remove any values
    // that are currently in use. Or do we care about that?
    // Let's test.
    //
    // Retrieve the configurations and save.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('genre_list', $genre_list)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
