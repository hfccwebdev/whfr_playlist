<?php

namespace Drupal\whfr_playlist\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the WHFR Program entity edit forms.
 */
class WhfrProgramForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();

    $message_arguments = ['%label' => $this->entity->label()];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New program %label has been created.', $message_arguments));
      $this->logger('whfr_playlist')->notice('Created new program %label', $message_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('Program %label has been updated.', $message_arguments));
      $this->logger('whfr_playlist')->notice('Updated program %label.', $message_arguments);
    }

    $form_state->setRedirect('entity.whfr_program.canonical', ['whfr_program' => $entity->id()]);
  }

}
