<?php

namespace Drupal\whfr_playlist\Plugin\Search;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\Config;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\search\Plugin\ConfigurableSearchPluginBase;
use Drupal\search\Plugin\SearchIndexingInterface;
use Drupal\search\SearchIndexInterface;
use Drupal\search\SearchQuery;
use Drupal\whfr_playlist\Entity\WhfrProgram;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a search plugin for WHFR Program entities.
 *
 * @SearchPlugin(
 *   id = "whfr_playlist_program_search",
 *   title = @Translation("WHFR Programs")
 * )
 */
class ProgramSearch extends ConfigurableSearchPluginBase implements AccessibleInterface, SearchIndexingInterface {

  use MessengerTrait;

  /**
   * A database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Stores the Search Index service.
   *
   * @var \Drupal\search\SearchIndexInterface
   */
  private $searchIndex;

  /**
   * A module manager object.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * A config object for 'search.settings'.
   *
   * @var \Drupal\Core\Config\Config
   */
  private $searchSettings;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * The Drupal account to use for checking for access to advanced search.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $account;

  /**
   * The Renderer service to format the username and entity.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * The list of options and info for advanced search filters.
   *
   * Each entry in the array has the option as the key and for its value, an
   * array that determines how the value is matched in the database query. The
   * possible keys in that array are:
   * - column: (required) Name of the database column to match against.
   * - join: (optional) Information on a table to join. By default the data is
   *   matched against any tables joined in the $query declaration in
   *   findResults().
   * - operator: (optional) OR or AND, defaults to OR.
   *
   * For advanced search to work, probably also must modify:
   * - buildSearchUrlQuery() to build the redirect URL.
   * - searchFormAlter() to add fields to advanced search form.
   *
   * Note: In our case joins aren't needed because the {contact} table is
   * joined in findResults().
   *
   * @var array
   */
  private $advanced = [
    'inactive' => [
      'column' => 'p.inactive',
    ],
  ];

  /**
   * A constant for setting and checking the query string.
   */
  const ADVANCED_FORM = 'advanced-form';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('search.index'),
      $container->get('module_handler'),
      $container->get('config.factory')->get('search.settings'),
      $container->get('language_manager'),
      $container->get('renderer'),
      $container->get('current_user')
    );
  }

  /**
   * Constructs \Drupal\content_entity_example\Plugin\Search\ContactSearch.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   A database connection object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\search\SearchIndexInterface $search_index
   *   The search index service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   A module manager object.
   * @param \Drupal\Core\Config\Config $search_settings
   *   A config object for 'search.settings'.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The Render service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The $account object to use for checking for access to advanced search.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    SearchIndexInterface $search_index,
    ModuleHandlerInterface $module_handler,
    Config $search_settings,
    LanguageManagerInterface $language_manager,
    RendererInterface $renderer,
    AccountInterface $account = NULL
  ) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->searchIndex = $search_index;
    $this->moduleHandler = $module_handler;
    $this->searchSettings = $search_settings;
    $this->languageManager = $language_manager;
    $this->renderer = $renderer;
    $this->account = $account;
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->addCacheTags(['whfr_programs']);
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowedIfHasPermission($account, 'access content');
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function isSearchExecutable() {
    // Contact search is executable if we have keywords or an advanced
    // parameter.
    // At least, we should parse out the parameters and see if there are any
    // keyword matches in that case, rather than just printing out the
    // "Please enter keywords" message.
    return !empty($this->keywords) || (isset($this->searchParameters['inactive']) && count($this->searchParameters['inactive']));
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    if ($this->isSearchExecutable()) {
      $results = $this->findResults();

      if ($results) {
        return $this->prepareResults($results);
      }
    }

    return [];
  }

  /**
   * Queries to find search results, and sets status messages.
   *
   * This method can assume that $this->isSearchExecutable() has already been
   * checked and returned TRUE.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   Results from search query execute() method, or NULL if the search
   *   failed.
   */
  protected function findResults() {

    $keys = $this->keywords;

    // Build matching conditions.
    $query = $this->database
      ->select('search_index', 'i')
      ->extend('Drupal\search\SearchQuery')
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender');

    $query->searchExpression($keys, $this->getPluginId());

    // Run the query.
    $find = $query
      // Add the language code of the indexed item to the result of the query,
      // since the entity will be rendered using the respective language.
      ->fields('i', ['langcode'])
      // And since SearchQuery makes these into GROUP BY queries, if we add
      // a field, for PostgreSQL we also need to make it an aggregate or a
      // GROUP BY. In this case, we want GROUP BY.
      ->groupBy('i.langcode')
      ->limit(10)
      ->execute();

    // Check query status and set messages if needed.
    $status = $query->getStatus();

    if ($status & SearchQuery::EXPRESSIONS_IGNORED) {
      $this->messenger()->addWarning($this->t(
        'Your search used too many AND/OR expressions. Only the first @count terms were included in this search.',
        ['@count' => $this->searchSettings->get('and_or_limit')]
      ));
    }

    if ($status & SearchQuery::LOWER_CASE_OR) {
      $this->messenger()->addWarning($this->t(
        'Search for either of the two terms with uppercase <strong>OR</strong>. For example, <strong>cats OR dogs</strong>.
      '));
    }

    if ($status & SearchQuery::NO_POSITIVE_KEYWORDS) {
      $this->messenger()->addWarning($this->formatPlural(
        $this->searchSettings->get('index.minimum_word_size'),
        'You must include at least one keyword to match in the content, and punctuation is ignored.',
        'You must include at least one keyword to match in the content. Keywords must be at least @count characters, and punctuation is ignored.'
      ));
    }

    return $find;
  }

  /**
   * Prepares search results for rendering.
   *
   * @param \Drupal\Core\Database\StatementInterface $found
   *   Results found from a successful search query execute() method.
   *
   * @return array
   *   Array of search result item render arrays (empty array if no results).
   */
  protected function prepareResults(StatementInterface $found) {
    $results = [];

    $entity_storage = $this->entityTypeManager->getStorage('whfr_program');
    $entity_render = $this->entityTypeManager->getViewBuilder('whfr_program');
    $keys = $this->keywords;

    foreach ($found as $item) {
      // Render the entity.
      $entity = $entity_storage->load($item->sid)->getTranslation($item->langcode);
      $build = $entity_render->view($entity, 'search_result', $item->langcode);

      unset($build['#theme']);

      // Build the snippet.
      $rendered = $this->renderer->renderPlain($build);
      $this->addCacheableDependency(CacheableMetadata::createFromRenderArray($build));

      // Allow other modules to add to snippet.
      if ($update = $this->moduleHandler->invokeAll('whfr_playlist_update_index', [$entity])) {
        $rendered .= ' ' . implode(' ', $update);
      }
      $extra = $this->moduleHandler->invokeAll('whfr_playlist_search_result', [$entity]);

      $language = $this->languageManager->getLanguage($item->langcode);

      $result = [
        'link' => $entity->toUrl(
          'canonical',
          ['absolute' => TRUE, 'language' => $language]
        )->toString(),
        'type' => 'WHFR Program',
        'title' => $entity->label(),
        'entity' => $entity,
        'extra' => $extra,
        'score' => $item->calculated_score,
        'snippet' => search_excerpt($keys, $rendered, $item->langcode),
        'langcode' => $entity->language()->getId(),
      ];

      $this->addCacheableDependency($entity);

      $results[] = $result;
    }
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex() {
    // Interpret the cron limit setting as the maximum number
    // of entities to index per cron run.
    $limit = (int) $this->searchSettings->get('index.cron_limit');

    $result = $this->database->queryRange("
      SELECT p.id, MAX(s.reindex) FROM {whfr_program} p
      LEFT JOIN {search_dataset} s ON s.sid = p.id AND s.type = :type
      WHERE s.sid IS NULL OR s.reindex <> 0
      GROUP BY p.id
      ORDER BY MAX(s.reindex) is null DESC, MAX(s.reindex) ASC, p.id ASC
      ", 0, $limit, [':type' => $this->getPluginId()]);

    $rids = $result->fetchCol();
    if (!$rids) {
      return;
    }

    $entity_storage = $this->entityTypeManager->getStorage('whfr_program');

    foreach ($entity_storage->loadMultiple($rids) as $entity) {
      $this->indexEntity($entity);
    }
  }

  /**
   * Indexes a single contact.
   *
   * @param \Drupal\whfr_playlist\Entity\WhfrProgram $entity
   *   The contact to index.
   */
  protected function indexEntity(WhfrProgram $entity) {
    $languages = $entity->getTranslationLanguages();
    $entity_render = $this->entityTypeManager->getViewBuilder('whfr_program');

    foreach ($languages as $language) {
      $entity = $entity->getTranslation($language->getId());
      // Render the contact.
      $build = $entity_render->view($entity, 'search_index', $language->getId());

      unset($build['#theme']);

      // Add the title to text so it is searchable.
      $build['search_title'] = [
        '#prefix' => '<h1>',
        '#plain_text' => $entity->label(),
        '#suffix' => '</h1>',
        '#weight' => -1000,
      ];
      $text = $this->renderer->renderPlain($build);

      // Fetch extra data normally not visible.
      if ($extra = $this->moduleHandler->invokeAll('whfr_program_update_index', [$entity])) {
        $text .= ' ' . implode(' ', $extra);
      }

      // Update index, using search index "type" equal to the plugin ID.
      $this->searchIndex->index($this->getPluginId(), $entity->id(), $language->getId(), $text);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function indexClear() {
    // All entities share a common search index "type" equal to
    // the plugin ID.
    $this->searchIndex->clear($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function markForReindex() {
    // All entities share a common search index "type" equal to
    // the plugin ID.
    $this->searchIndex->markForReindex($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function indexStatus() {

    $total = $this->database->query("SELECT COUNT(id) FROM {whfr_program}")->fetchField();
    $remaining = $this->database->query("
      SELECT COUNT(DISTINCT p.id) FROM {whfr_program} p
      LEFT JOIN {search_dataset} s ON s.sid = p.id AND s.type = :type
      WHERE s.sid IS NULL OR s.reindex <> 0
      ", [':type' => $this->getPluginId()])->fetchField();

    return ['remaining' => $remaining, 'total' => $total];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
