<?php

namespace Drupal\whfr_playlist;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

/**
 * Defines the whfr_playlist schema handler.
 */
class WhfrPlaylistStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    if ($base_table = $this->storage->getBaseTable()) {
      $schema[$base_table]['indexes'] += [
        'artist' => ['artist'],
        'song' => ['song'],
        'album' => ['album'],
        'record_label' => ['record_label'],
        'genre' => ['genre'],
        'created' => ['created'],
        'changed' => ['changed'],
      ];
    }
    return $schema;
  }

}
