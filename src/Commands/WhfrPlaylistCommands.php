<?php

namespace Drupal\whfr_playlist\Commands;

use Drush\Commands\DrushCommands;
use Drupal\whfr_playlist\WhfrPlaylistImportService;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class WhfrPlaylistCommands extends DrushCommands {

  /**
   * Stores the playlist import service.
   *
   * @var \Drupal\whfr_playlist\WhfrPlaylistImportService
   */
  protected $import;

  /**
   * Class constructor.
   *
   * @param \Drupal\whfr_playlist\WhfrPlaylistImportService $whfr_import_service
   *   The playlist import service.
   */
  public function __construct(WhfrPlaylistImportService $whfr_import_service) {
    $this->import = $whfr_import_service;
  }

  /**
   * Import playlist programs.
   *
   * @validate-module-enabled whfr_playlist
   *
   * @command whfr:programs-import
   * @aliases wpri
   */
  public function importPrograms() {
    $this->import->importPrograms();
  }

  /**
   * Import recent playlist entries.
   *
   * @validate-module-enabled whfr_playlist
   *
   * @command whfr:playlist-import
   * @aliases wpli
   */
  public function importPlaylist() {
    echo $this->import->importPlaylist();
  }

  /**
   * Import all playlist entries from csv.
   *
   * @param string $filename
   *   The file to import.
   * @param array $options
   *   An associative array of options whose values
   *   come from cli, aliases, config, etc.
   *
   * @option limit
   *   Number of entries to import.
   * @usage drush whfr:playlist-full /tmp/playlist.csv --limit=200
   *   Imports playlist entries from a file, with optional limit.
   * @validate-module-enabled whfr_playlist
   *
   * @command whfr:playlist-full
   */
  public function fileImport(string $filename, array $options = ['limit' => 0]) {
    $this->import->fileImport($filename, $options);
  }

}
