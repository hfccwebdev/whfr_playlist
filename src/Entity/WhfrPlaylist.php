<?php

namespace Drupal\whfr_playlist\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the WHFR Playlist content entity type.
 *
 * @ingroup whfr_playlist
 *
 * @ContentEntityType(
 *   id = "whfr_playlist",
 *   label = @Translation("WHFR Playlist"),
 *   label_collection = @Translation("WHFR Playlist"),
 *   handlers = {
 *     "storage_schema" = "Drupal\whfr_playlist\WhfrPlaylistStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\whfr_playlist\Form\WhfrPlaylistForm",
 *       "edit" = "Drupal\whfr_playlist\Form\WhfrPlaylistForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\whfr_playlist\WhfrPlaylistAccessControlHandler",
 *   },
 *   base_table = "whfr_playlist",
 *   revision_table = "whfr_playlist_revision",
 *   show_revision_ui = TRUE,
 *   translatable = FALSE,
 *   admin_permission = "administer whfr playlist",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *     "datestamp" = "datestamp",
 *   },
 *   revision_metadata_keys = {
 *     "revision_default" = "revision_default",
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/playlist/{whfr_playlist}",
 *     "edit-form" = "/playlist/{whfr_playlist}/edit",
 *     "version-history" = "/playlist/{whfr_playlist}/revisions",
 *     "revision" = "/playlist/{whfr_playlist}/revisions/{whfr_playlist_revision}/view",
 *     "add-form" = "/playlist/add",
 *   },
 *   field_ui_base_route = "entity.whfr_playlist.settings"
 * )
 */
class WhfrPlaylist extends RevisionableContentEntityBase implements EntityOwnerInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function label() {
    $datestamp = $this->getFormattedTimestamp();
    $artist = $this->get('artist')->value;
    $song = $this->get('song')->value;
    return implode(' ', [$datestamp, $artist, $song]);
  }

  /**
   * Gets the formatted datestamp.
   *
   * @return string
   *   The date and time the song was played.
   */
  private function getFormattedTimestamp() {
    // @todo Needs work. Why did I write it this way?
    // Can we use view() method instead?
    $datestamp = strtotime(gmdate($this->datestamp->value));

    // We cannot inject dependencies into an entity.
    // @see https://www.drupal.org/i/2142515
    return \Drupal::service('date.formatter')->format($datestamp, 'short');
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {

    // We cannot inject dependencies here, so get the timestamp,
    // then populate created and changed timestamp values.
    $request_time = \Drupal::service('datetime.time')->getRequestTime();
    if (empty($this->get('created')->value)) {
      $this->set('created', $request_time);
    }
    $this->set('changed', $request_time);

    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Reindex the entity when it is updated. The entity is automatically
    // indexed when it is added, simply by being added to the {contact} table.
    // Only required if using the core search index.
    // We cannot inject dependencies here, so call services directly.
    if ($update) {
      if (\Drupal::moduleHandler()->moduleExists('search')) {
        \Drupal::service('search.index')->markForReindex('whfr_playlist_playlist_search', $this->id());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['datestamp'] = BaseFieldDefinition::create('datetime')
      ->setLabel(new TranslatableMarkup('Date/Time'))
      ->setDescription(new TranslatableMarkup('The date and time the song was played.'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'datetime_default',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['artist'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Artist'))
      ->setDescription(new TranslatableMarkup('Stores the artist.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['song'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Song'))
      ->setDescription(new TranslatableMarkup('Stores the song title.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['album'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Album'))
      ->setDescription(new TranslatableMarkup('Stores the album title.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['record_label'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Label'))
      ->setDescription(new TranslatableMarkup('Stores the record label info.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['genre'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Genre'))
      ->setDescription(new TranslatableMarkup('Stores the genre.'))
      ->setSettings([
        // Use hook_entity_base_field_info_alter()
        // to make these values configurable.
        'allowed_values' => [
          'any' => 'any',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // We need to keep this for old entries that don't have a valid program.
    $fields['program_title'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Program Title'))
      ->setDescription(new TranslatableMarkup('Stores the show title.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 6,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['program_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Program'))
      ->setDescription(new TranslatableMarkup('Stores the id of the show.'))
      ->setSetting('target_type', 'whfr_program')
      ->setSetting('max_length', 11)
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Created On'))
      ->setDescription(new TranslatableMarkup('Stores the creation date of this entry.'));

    $fields['changed'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('Stores the most recent change date of this entry.'));

    // The uid field is defined by ownerBaseFieldDefinitions() above.
    $fields['uid']
      ->setLabel(new TranslatableMarkup('Authored by'))
      ->setDescription(new TranslatableMarkup('The username of the content author.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 99,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
          'weight' => 99,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['revision_default'] = BaseFieldDefinition::create('boolean')
      ->setName('revision_default')
      ->setLabel(new TranslatableMarkup('Default revision'))
      ->setDescription(new TranslatableMarkup('A flag indicating whether this was a default revision when it was saved.'))
      ->setStorageRequired(TRUE)
      ->setInternal(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(TRUE);

    return $fields;
  }

}
