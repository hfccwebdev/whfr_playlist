<?php

namespace Drupal\whfr_playlist\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the WHFR Program content entity type.
 *
 * @ingroup whfr_playlist
 *
 * @ContentEntityType(
 *   id = "whfr_program",
 *   label = @Translation("WHFR Program"),
 *   label_collection = @Translation("WHFR Programs"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\whfr_playlist\Form\WhfrProgramForm",
 *       "edit" = "Drupal\whfr_playlist\Form\WhfrProgramForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\whfr_playlist\WhfrProgramAccessControlHandler",
 *   },
 *   base_table = "whfr_program",
 *   revision_table = "whfr_program_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer whfr program",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "label" = "title",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_default" = "revision_default",
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/programs/{whfr_program}",
 *     "edit-form" = "/programs/{whfr_program}/edit",
 *     "delete-form" = "/programs/{whfr_program}/delete",
 *     "version-history" = "/programs/{whfr_program}/revisions",
 *     "revision" = "/programs/{whfr_program}/revisions/{whfr_program_revision}/view",
 *     "add-form" = "/programs/add",
 *   },
 *   field_ui_base_route = "entity.whfr_program.settings"
 * )
 */
class WhfrProgram extends RevisionableContentEntityBase implements EntityOwnerInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {

    // We cannot inject dependencies here, so get the timestamp,
    // then populate created and changed timestamp values.
    $request_time = \Drupal::service('datetime.time')->getRequestTime();
    if (empty($this->get('created')->value)) {
      $this->set('created', $request_time);
    }
    $this->set('changed', $request_time);

    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Reindex the entity when it is updated. The entity is automatically
    // indexed when it is added, simply by being added to the {contact} table.
    // Only required if using the core search index.
    // We cannot inject dependencies here, so call services directly.
    if ($update) {
      if (\Drupal::moduleHandler()->moduleExists('search')) {
        \Drupal::service('search.index')->markForReindex('whfr_playlist_program_search', $this->id());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the WHFR Program.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['inactive'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Inactive'))
      ->setDescription(t('Select this to mark a program inactive.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -4,
      ])
      ->setDisplayOptions('view', [
        'label' => 'boolean',
        'type' => 'number_unformatted',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['genre'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Genre'))
      ->setDescription(new TranslatableMarkup('Stores the genre.'))
      ->setSettings([
        // Use hook_entity_base_field_info_alter()
        // to make these values configurable.
        'allowed_values' => [
          'any' => 'any',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['legacy_nid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Legacy NID'))
      ->setDescription(t('Stores the legacy node ID of the show.'))
      ->setDisplayOptions('form', [
        'type' => 'number',
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_unformatted',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Created On'))
      ->setDescription(new TranslatableMarkup('Stores the creation date of this entry.'));

    $fields['changed'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('Stores the most recent change date of this entry.'));

    // The uid field is defined by ownerBaseFieldDefinitions() above.
    $fields['uid']
      ->setLabel(new TranslatableMarkup('Authored by'))
      ->setDescription(new TranslatableMarkup('The username of the content author.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 99,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
          'weight' => 99,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['revision_default'] = BaseFieldDefinition::create('boolean')
      ->setName('revision_default')
      ->setLabel(new TranslatableMarkup('Default revision'))
      ->setDescription(new TranslatableMarkup('A flag indicating whether this was a default revision when it was saved.'))
      ->setStorageRequired(TRUE)
      ->setInternal(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(TRUE);

    return $fields;
  }

}
