<?php

namespace Drupal\whfr_playlist\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class WhfrPlaylistRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // This should become unnecessary if "collection permission" gets added.
    // @see https://www.drupal.org/project/drupal/issues/2953566
    // Define custom access for '/admin/content/whfr-playlist'.
    if ($route = $collection->get('entity.whfr_playlist.collection')) {
      $route->setRequirement('_permission', 'access whfr overview');
    }
    // Define custom access for '/admin/content/whfr-program'.
    if ($route = $collection->get('entity.whfr_program.collection')) {
      $route->setRequirement('_permission', 'access whfr overview');
    }
  }

}
