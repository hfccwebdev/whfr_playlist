<?php

namespace Drupal\whfr_playlist;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\whfr_playlist\Entity\WhfrPlaylist;
use Drupal\whfr_playlist\Entity\WhfrProgram;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

/**
 * Defines the WHFR Playlist Import Service.
 */
class WhfrPlaylistImportService {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Module configuration settings name.
   */
  const MODULE_SETTINGS = 'whfr_playlist.settings';

  /**
   * Import configuration settings name.
   *
   * @var string
   */
  const IMPORT_SETTINGS = 'whfr_playlist.import_settings';

  /**
   * Stores the Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Stores the Date Formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  private $dateFormatter;

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Stores the GuzzleHttp Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The Date Formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP Client.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TimeInterface $time,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    ClientInterface $http_client
  ) {
    $this->configFactory = $config_factory;
    $this->time = $time;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
  }

  /**
   * Import playlist programs.
   */
  public function importPrograms() {

    $data = $this->getData('programs_url');
    $count = 0;

    foreach ($data as $item) {
      $result = $this->entityTypeManager->getStorage('whfr_program')->getQuery()
        ->accessCheck(FALSE)
        ->condition('legacy_nid', $item->nid)
        ->execute();
      if (empty($result)) {
        $program = WhfrProgram::create();
        $program->title = $item->title;
        $program->legacy_nid = $item->nid;
        $program->inactive = FALSE;
        $program->uid = $this->importSettings('default_owner');
        $program->save();
        $count++;
      }
    }
    $this->messenger()->addMessage($this->t(
      'Imported @count programs.',
      ['@count' => $count]
    ));
  }

  /**
   * Import recent playlist entries.
   */
  public function importPlaylist() {

    $data = $this->getData('playlist_url');
    $count = 0;

    foreach ($data as $item) {
      $datestamp = $this->dateFormatter->format($item->datestamp, 'custom', 'Y-m-d\TH:i:s', 'UTC');

      $result = $this->entityTypeManager->getStorage('whfr_playlist')->getQuery()
        ->accessCheck(FALSE)
        ->condition('datestamp', $datestamp)
        ->execute();

      if (empty($result)) {
        $entry = WhfrPlaylist::create();
        $entry->uid = $this->importSettings('default_owner');
        $entry->datestamp = $datestamp;
        $entry->artist = $item->artist ?? NULL;
        $entry->song = $item->song ?? NULL;
        $entry->album = $item->album ?? NULL;
        $entry->record_label = $item->label ?? NULL;
        $entry->genre = $this->checkGenre($item->genre);
        $entry->program_title = $item->program_title ?? NULL;
        $entry->program_id = $this->checkProgram($item->program_nid);
        $entry->create = $item->created;
        $entry->changed = $item->changed;
        $entry->save();
        $count++;
        usleep(1000);
      }
    }
    $this->messenger()->addMessage($this->t(
      'Imported @count entries.',
      ['@count' => $count]
    ));
  }

  /**
   * Import all playlist entries from csv.
   *
   * @param string $filename
   *   The file to import.
   * @param array $options
   *   An associative array of options whose values
   *   come from cli, aliases, config, etc.
   */
  public function fileImport(string $filename, array $options): void {

    ini_set('auto_detect_line_endings', TRUE);
    $count = 0;

    $limit = intval($options['limit'] ?? 0);
    if ($limit === 0) {
      $limit = $this->importSettings('file_import_limit') ?? 5000;
    }

    $handle = @fopen($filename, 'r');
    while (($count < $limit) && ($row = fgetcsv($handle))) {
      if ($row[0] == 'nid') {
        $fieldnames = $row;
        continue;
      }

      $values = [];
      foreach ($row as $key => $value) {
        $values[$fieldnames[$key]] = trim($value);
      }

      $datestamp = $this->dateFormatter->format($values['datestamp'], 'custom', 'Y-m-d\TH:i:s');

      $result = $this->entityTypeManager->getStorage('whfr_playlist')->getQuery()
        ->accessCheck(FALSE)
        ->condition('datestamp', $datestamp)
        ->execute();

      if (empty($result)) {
        if (isset($values['program_nid'])) {
          $program_nid = $this->checkProgram(intval($values['program_nid']));
        }
        else {
          $program_nid = NULL;
        }
        $entry = WhfrPlaylist::create();
        $entry->uid = $this->importSettings('default_owner');
        $entry->datestamp = $datestamp;
        $entry->artist = $values['artist'] ?? NULL;
        $entry->song = $values['song'] ?? NULL;
        $entry->album = $values['album'] ?? NULL;
        $entry->record_label = $values['label'] ?? NULL;
        $entry->genre = $this->checkGenre($values['genre']);
        $entry->program_title = $values['program_title'] ?? NULL;
        $entry->program_id = $program_nid;
        $entry->create = $values['created'];
        $entry->changed = $values['changed'];
        $entry->save();
        $count++;
        usleep(1000);
      }
    }
    fclose($handle);
    $this->messenger()->addMessage($this->t(
      'Imported @count entries.',
      ['@count' => $count]
    ));
  }

  /**
   * Get import settings.
   *
   * @param string $key
   *   The settings key to retrieve.
   */
  private function importSettings(string $key) {
    $settings = $this->configFactory->get(static::IMPORT_SETTINGS);
    return $settings->get($key) ?? NULL;
  }

  /**
   * Get data from WHFR API endpoints.
   */
  private function getData($endpoint) {
    $url = $this->importSettings($endpoint);
    $response = $this->httpClient->request('GET', $url, [RequestOptions::TIMEOUT => 60]);
    if ($response->getStatusCode() == 200) {
      $body = $response->getBody();
      return json_decode($body);
    }
    else {
      $this->watchdog()->error(
        'WHFR @endpoint API response @status: @message.',
        [
          '@endpoint' => $url,
          '@status' => $response->getStatusCode(),
          '@message' => $response->getReasonPhrase(),
        ]
      );
      return [];
    }
  }

  /**
   * Check available genre list for a match.
   *
   * @param string $search
   *   The genre name to check.
   *
   * @return string|null
   *   The settings key for the matched genre or null.
   */
  private function checkGenre(string $search): ?string {
    $genre_list = &drupal_static(__FUNCTION__);
    if (!isset($genre_list)) {
      $genre_list = $this->configFactory->get(static::MODULE_SETTINGS)->get('genre_list');
    }
    if (isset($genre_list[$search])) {
      return $search;
    }
    if ($key = array_search($search, $genre_list)) {
      return $key;
    }
    return NULL;
  }

  /**
   * Check available legacy program nids for a match.
   *
   * @param int $search
   *   The legacy program ID.
   *
   * @return int|null
   *   The ID of the matched whfr_program or null.
   */
  public function checkProgram(int $search): ?int {
    $programs = &drupal_static(__FUNCTION__);
    if (empty($search)) {
      return NULL;
    }
    if (isset($programs[$search])) {
      return $programs[$search];
    }

    $result = $this->entityTypeManager->getStorage('whfr_program')->getQuery()
      ->accessCheck(FALSE)
      ->condition('legacy_nid', $search)
      ->execute();

    if (!empty($result)) {
      $result = reset($result);
      $programs[$search] = $result;
      return $result;
    }
    return NULL;
  }

}
